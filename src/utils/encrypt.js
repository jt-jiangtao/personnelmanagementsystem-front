import CryptoJS from 'crypto-js'

let key = 'HAHG27638FGAjhssv38&WGSYui27dghFw8FFYu'
let iv = '26743892782201728'

function getAesString(data, key, iv) {
  key = CryptoJS.enc.Utf8.parse(key)
  iv = CryptoJS.enc.Utf8.parse(iv)
  let encrypted = CryptoJS.AES.encrypt(data, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7,
  })
  return encrypted.toString()
}
function getDAesString(encrypted, key, iv) {
  key = CryptoJS.enc.Utf8.parse(key)
  iv = CryptoJS.enc.Utf8.parse(iv)
  let decrypted = CryptoJS.AES.decrypt(encrypted, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7,
  })
  return decrypted.toString(CryptoJS.enc.Utf8)
}

export function getAES(data) {
  let encrypted = getAesString(data, key, iv)
  let encrypted1 = CryptoJS.enc.Utf8.parse(encrypted)
  return encrypted1
}

export function getDAes(data) {
  let decryptedStr = getDAesString(data, key, iv)
  return decryptedStr
}
