import Antd, { message } from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'
import { warning } from 'ant-design-vue/es/vc-util/warning'

export default function (app) {
  app.use(Antd)
  app.$message = message
  app.$confirm = confirm
  app.$warning = warning
}
