export const setLogInfo = function (data) {
  let info = ''
  if (typeof data === 'object') {
    info = JSON.stringify(data)
  }
  window.localStorage.setItem('logInfo', info)
}

export const getLogInfo = function () {
  let info = window.localStorage.getItem('logInfo')
    ? window.localStorage.getItem('logInfo')
    : ''
  return info ? JSON.parse(info) : ''
}

export const clearLogInfo = function () {
  window.localStorage.removeItem('logInfo')
}

export const clearCookie = function () {
  window.localStorage.clear()
}
