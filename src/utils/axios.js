import axios from 'axios'
import qs from 'qs'
import { getLogInfo } from '@/utils/cookie'

function baseUrl() {
  return process.env.VUE_APP_API_ENVIRONMENT === 'MOCKS'
    ? '/mocks'
    : 'http://182.254.151.191:8888/api'
}

const createAxios = config => {
  let instance = axios.create(config)
  instance.interceptors.request.use(async config => {
    let token = getLogInfo()['token']
    config.data.token = token
    if (config.method === 'post') {
      config.data = qs.stringify(config.data)
    }
    return config
  })
  // instance.defaults.headers = {
  //   'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
  // }
  return instance
}

const API = createAxios({
  baseURL: baseUrl(),
  timeout: 10000,
})

export default API
