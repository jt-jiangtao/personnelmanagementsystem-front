import { createApp } from 'vue'
import App from './layout/App.vue'
import router from './router'
import store from './store'
import Antd from './utils/antd'
import './utils/axios'
import './mocks'

const app = createApp(App)
app.config.productionTip = false
app.use(store).use(router).use(Antd)
app.mount('#app')
