export default {
  // 添加员工
  'POST /staff/add/': function (option) {
    let body = option.body
    return {
      code: 200,
      data: {
        name: '吴系挂',
        body,
      },
      msg: '',
    }
  },
  // 删除员工
  'POST /staff/delete/': function (option) {
    let body = option.body
    return {
      code: 200,
      data: {
        name: '吴系挂',
        body,
      },
      msg: '',
    }
  },
  // 修改员工信息
  'POST /staff/update/': function (option) {
    let body = option.body
    let data = {
      code: 200,
      data: {
        body,
      },
      msg: '',
    }
    return data
  },
  // 获取全部员工信息
  'POST /staff/searchAll/': function (option) {
    let body = option.body
    return {
      code: 200,
      data: {
        staff: [
          {
            id: '201827397',
            name: 'aiangtao',
            sex: '女',
            degree: '本科',
            department: '招生处',
            gradualSchool: '中南民族大学',
            healthy: 0,
            post: '职称：无',
            duty: '招生11111111111111111111111111111111111111111111111111111111111111',
          },
          {
            id: '201827394',
            name: 'ciangtao',
            sex: '男',
            degree: '本科',
            department: '招生处',
            gradualSchool: '中南民族大学',
            healthy: 1,
            post: '职称：无',
            duty: '招生',
          },
          {
            id: '201827391',
            name: 'jiangtao',
            sex: '男',
            degree: '硕士',
            department: '招生处',
            gradualSchool: '中南民族大学',
            healthy: 2,
            post: '职称：无',
            duty: '招生',
          },
          {
            id: '201827399',
            name: 'biangtao',
            sex: '男',
            degree: '本科',
            department: '招生处',
            gradualSchool: '中南民族大学',
            healthy: 2,
            post: '职称：无',
            duty: '招生',
          },
        ],
        num: 4,
        body,
      },
      msg: 'xxx',
    }
  },
  // 根据姓名检索
  'POST /staff/searchByName/': function (option) {
    let body = option.body
    return {
      code: 200,
      data: {
        staff: [
          {
            id: '201827397',
            name: 'jiangtao',
            sex: '女',
            degree: '本科',
            department: '招生处',
            gradualSchool: '中南民族大学',
            healthy: '健康',
            post: '职称：无',
            duty: '招生11111111111111111111111111111111111111111111111111111111111111',
          },
          {
            id: '201827394',
            name: 'jiangtao',
            sex: '男',
            degree: '本科',
            department: '招生处',
            gradualSchool: '中南民族大学',
            healthy: '健康',
            post: '职称：无',
            duty: '招生',
          },
          {
            id: '201827391',
            name: 'jiangtao',
            sex: '男',
            degree: '本科',
            department: '招生处',
            gradualSchool: '中南民族大学',
            healthy: '健康',
            post: '职称：无',
            duty: '招生',
          },
          {
            id: '201827399',
            name: 'jiangtao',
            sex: '男',
            degree: '硕士',
            department: '招生处',
            gradualSchool: '中南民族大学',
            healthy: '健康',
            post: '职称：无',
            duty: '招生',
          },
        ],
        num: 4,
        body,
      },
      msg: 'xxx',
    }
  },
}
