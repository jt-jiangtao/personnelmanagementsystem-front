import Mock from 'mockjs'
Mock.XHR.prototype.withCredentials = true // Hack 代码, mock 拦截 XHR 后会丢失 cookie

/**
 * 接口 Mock，在以下注册过的接口才会被拦截
 * mock接口的编写示例：
 *
 * export default {
 *   '/api/v1/auth/check': {
 *    'total|1-100': 0,
 *    'list|1-100': [{}]
 *    },
 *   'POST /api/v1/user/$id': () => ({})
 * }
 *
 * key 是url的定义，value 是数据定义
 *
 * url 的定义可以指定请求方法，也可以不指定，如果不指定则为拦截全部方法，url内容是正则表达
 * 式，如果是不规范的表达式，视为全匹配
 *
 * value 可以是函数或者对象，如果是函数则需要返回 mockjs 的数据模板，mockjs 的数据模板示例
 * 参考：http://mockjs.com/examples.html
 *
 */
let mockApi = []

/**
 * 这是全局需要mock的数据，跟具体业务走的数据建议在具体的组件或者在具体的单元测试中去引用
 */
mockApi =
  process.env.VUE_APP_API_ENVIRONMENT === 'MOCKS'
    ? [
        require('./login'),
        require('./research'),
        require('./stuff'),
        require('./teach'),
      ]
    : []

export default function mock(api) {
  // require es module
  if (api.default) {
    api = api.default
  }
  if (!api || typeof api != 'object') {
    return
  }

  Object.entries(api).forEach(([k, v]) => {
    let [method, url] = k.trim().split(' ')
    if (!url) {
      url = method
      method = undefined
    }
    if (method) {
      method = method.toLowerCase()
    }

    try {
      // 设置一些简写
      let hackUrl = url.trim()
      hackUrl = url.replace(/\/\*/g, '/.*') // 支持 * 通配符
      hackUrl = hackUrl.replace(/\/[$:@][a-z0-9_]*/gi, '/.*') // 支持url参数如 $id
      hackUrl = hackUrl.replace(/\/\{[a-z0-9_]*\}/gi, '/.*') // 支持 /user/{user_id} 这种参数
      url = new RegExp(hackUrl + '($|\\?)', 'im')
    } catch (e) {
      //nothing
    }

    Mock.mock(url, method, v)
    // console.debug('[MockAPI]: ' + k);
  })
}

mockApi.forEach(mock)
