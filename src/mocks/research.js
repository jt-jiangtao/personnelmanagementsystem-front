export default {
  // 添加研究信息
  'POST /teacher/research/add/': function (option) {
    let body = option.body
    return {
      code: 200,
      data: {
        body,
      },
      msg: '',
    }
  },
  // 删除研究信息
  'POST /teacher/research/delete/': function (option) {
    let body = option.body
    return {
      code: 200,
      data: {
        name: '吴系挂',
        body,
      },
      msg: '',
    }
  },
  // 获取全部研究信息
  'POST /teacher/research/all/': function (option) {
    let body = option.body
    return {
      code: 200,
      data: {
        courses: [
          {
            id: '201827391',
            teacherId: '1221',
            name: 'aiangtao',
            direction: 'direction',
            status: 0,
            patent: 'condition',
            paper: 'paper',
          },
          {
            id: '201827398',
            teacherId: '1221',
            name: 'biangtao',
            direction: 'direction',
            status: 1,
            patent: 'condition',
            paper: 'paper',
          },
          {
            id: '201827392',
            teacherId: '1221',
            name: 'ciangtao',
            direction: 'direction',
            status: 2,
            patent: 'condition',
            paper: 'paper',
          },
          {
            id: '201827395',
            teacherId: '1221',
            name: 'adiangtao',
            direction: 'direction',
            status: 2,
            patent: 'condition',
            paper: 'paper',
          },
          {
            id: '201827390',
            teacherId: '1221',
            name: 'jiangtao',
            direction: 'direction',
            status: 1,
            patent: 'condition',
            paper: 'paper',
          },
        ],
        num: 15,
        body,
      },
      msg: 'xxx',
    }
  },
}
