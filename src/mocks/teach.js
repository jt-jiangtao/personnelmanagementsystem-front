export default {
  // 添加教学信息
  'POST /teacher/course/add/': function (option) {
    let body = option.body
    return {
      code: 200,
      data: {
        body,
      },
      msg: '',
    }
  },
  // 删除教学信息
  'POST /teacher/course/delete/': function (option) {
    let body = option.body
    return {
      code: 200,
      data: {
        name: '吴系挂',
        body,
      },
      msg: '',
    }
  },
  // 获取全部教学信息
  'POST /teacher/course/all/': function (option) {
    let body = option.body
    return {
      code: 200,
      data: {
        courses: [
          {
            id: '123453',
            name: 'asd',
            teacherId: '111',
            time: '23',
            grade: '123',
            classProperty: '1234',
          },
          {
            id: '3456',
            name: 'sf',
            teacherId: 'af',
            time: 'a56',
            grade: '123',
            classProperty: '1234',
          },
        ],
        num: 15,
        body,
      },
      msg: 'xxx',
    }
  },
}
