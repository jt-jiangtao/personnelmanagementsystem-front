import API from '@/utils/axios'

export function addResearch(name, teacher, direction, status, patent, paper) {
  return API.post('/teacher/research/add/', {
    name,
    teacherId: teacher,
    direction,
    status,
    patent,
    paper,
  })
    .then(res => res.data)
    .catch(res => console.error(res))
}

export function deleteResearch(id) {
  return API.post('/teacher/research/delete/', { id })
    .then(res => res.data)
    .catch(res => console.error(res))
}

export function getAllResearch() {
  return API.post('/teacher/research/all/', {})
    .then(res => res.data)
    .catch(res => console.error(res))
}
