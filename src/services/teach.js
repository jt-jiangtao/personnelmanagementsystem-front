import API from '@/utils/axios'

export function addCourseInfo(name, teacher, time, grade, classProperty) {
  return API.post('/teacher/course/add/', {
    name,
    teacherId: teacher,
    time,
    grade,
    classProperty,
  })
    .then(res => res.data)
    .catch(res => console.error(res))
}

export function deleteCourseInfo(id) {
  return API.post('/teacher/course/delete/', {
    id,
  })
    .then(res => res.data)
    .catch(res => console.error(res))
}

export function getAllCourseInfo() {
  return API.post('/teacher/course/all/', {})
    .then(res => res.data)
    .catch(res => console.error(res))
}
