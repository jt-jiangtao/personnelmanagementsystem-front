import API from '@/utils/axios'

export function loginAIP(username, password) {
  return API.post('/admin/login/', { username, password })
    .then(res => res.data)
    .catch(res => console.error(res))
}

export function registerAPI(username, password, name, email) {
  return API.post('/admin/register/', { username, password, name, email })
    .then(res => res.data)
    .catch(res => console.error(res))
}

export function sendResetCode(email) {
  return API.post('/admin/sendEmail/', { email })
    .then(res => res.data)
    .catch(res => console.error(res))
}

export function resetPassword(email, code, password) {
  return API.post('/admin/obtainPassword/', { email, code, password })
    .then(res => res.data)
    .catch(res => console.error(res))
}
