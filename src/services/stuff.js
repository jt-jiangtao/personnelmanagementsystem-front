import API from '@/utils/axios'

export function addStuff(
  name,
  sex,
  degree,
  department,
  gradualSchool,
  healthy,
  post,
  duty
) {
  return API.post('/staff/add/', {
    name,
    sex,
    degree,
    department,
    gradualSchool,
    healthy,
    post,
    duty,
  })
    .then(res => res.data)
    .catch(res => console.error(res))
}

export function deleteStuff(id) {
  return API.post('/staff/delete/', { id })
    .then(res => res.data)
    .catch(res => console.error(res))
}

export function updateStuff(
  id,
  name,
  sex,
  degree,
  department,
  gradualSchool,
  healthy,
  post,
  duty
) {
  return API.post('/staff/update/', {
    id,
    name,
    sex,
    degree,
    department,
    gradualSchool,
    healthy,
    post,
    duty,
  })
    .then(res => res.data)
    .catch(res => console.error(res))
}

export function searchAllStuff() {
  return API.post('/staff/searchAll/', {})
    .then(res => res.data)
    .catch(res => console.error(res))
}

export function searchStuffByName(name) {
  return API.post('/staff/searchByName/', { name })
    .then(res => res.data)
    .catch(res => console.error(res))
}
