import { createRouter, createWebHistory } from 'vue-router'
import { getLogInfo } from '@/utils/cookie'
const Main = () => import('../pages/main/Index')
const Login = () => import('../pages/Login')
const Teachers = () => import('../pages/main/Teachers')
const Research = () => import('../pages/main/Research')
const Teach = () => import('../pages/main/Teach')

const routes = [
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/log',
    name: 'Login',
    component: Login,
    meta: {
      title: '登录注册',
    },
  },
  {
    path: '/home',
    name: 'Main',
    component: Main,
    children: [
      {
        path: '/home',
        name: 'Teachers',
        component: Teachers,
        meta: {
          title: '员工',
        },
      },
      {
        path: '/home/teach',
        name: 'Teach',
        component: Teach,
        meta: {
          title: '教学',
        },
      },
      {
        path: '/home/research',
        name: 'Research',
        component: Research,
        meta: {
          title: '科研',
        },
      },
    ],
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  base: '/',
  routes,
})

router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title
  }
  // 已登录不能到/log
  if (to.path === '/log' && getLogInfo().token) next('/home')
  // 未登录只能到/log
  if (!getLogInfo().token && to.path !== '/log') next('/log')
  // 路由无法匹配
  if (to.matched.length === 0) {
    from.path ? next({ path: from.path }) : next('/home') //如果上级也未匹配到路由则跳转主页面，如果上级能匹配到则转上级路由
  } else next()
})

export default router
