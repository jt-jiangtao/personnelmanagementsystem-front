import { createStore } from 'vuex'
import { getLogInfo, setLogInfo as setLog } from '@/utils/cookie'

export default createStore({
  state: {
    logInfo: getLogInfo(),
  },
  mutations: {
    setLogInfo(state, logInfo) {
      state.logInfo = logInfo
      setLog(logInfo)
    },
  },
  actions: {
    setLogInfo({ commit }, data) {
      commit('setLogInfo', data)
    },
  },
  modules: {},
})
