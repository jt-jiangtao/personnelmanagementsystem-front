export const rules = {
  id: [
    {
      required: true,
      message: '请填写员工编号',
      trigger: 'blur',
    },
  ],
  name: [
    {
      required: true,
      message: '请填写员工姓名',
      trigger: 'blur',
    },
  ],
  department: [
    {
      required: true,
      message: '请填写部门',
      trigger: 'blur',
    },
  ],
  degree: [
    {
      required: true,
      message: '请填写学历',
      trigger: 'blur',
    },
  ],
  sex: [
    {
      required: true,
      message: '请选择性别',
      trigger: 'blur',
    },
  ],
  gradualSchool: [
    {
      required: true,
      message: '请填写毕业院校',
      trigger: 'blur',
    },
  ],
  healthy: [
    {
      required: true,
      message: '请选择健康状态',
      trigger: 'blur',
      type: 'number',
    },
  ],
  post: [
    {
      required: true,
      message: '请填写员工职称',
      trigger: 'blur',
    },
  ],
  duty: [
    {
      required: true,
      message: '请填写员工职务',
      trigger: 'blur',
    },
  ],
}
