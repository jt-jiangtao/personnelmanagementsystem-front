export const rules = {
  id: [
    {
      required: true,
      message: '请填写课题编号',
      trigger: 'blur',
    },
  ],
  name: [
    {
      required: true,
      message: '请填写课题',
      trigger: 'blur',
    },
  ],
  teacherId: [
    {
      required: true,
      message: '请填写教师编号',
      trigger: 'blur',
    },
  ],
  direction: [
    {
      required: true,
      message: '研究方向',
      trigger: 'blur',
    },
  ],
  status: [
    {
      required: true,
      message: '请填写课程研究情况',
      trigger: 'blur',
    },
  ],
  patent: [
    {
      required: true,
      message: '请填写专利',
      trigger: 'blur',
    },
  ],
  paper: [
    {
      required: true,
      message: '论文发表',
      trigger: 'blur',
    },
  ],
}
