export const rules = {
  name: [
    {
      required: true,
      message: '请填写课程名称',
      trigger: 'blur',
    },
  ],
  teacherId: [
    {
      required: true,
      message: '请填写任课老师编号',
      trigger: 'blur',
    },
  ],
  time: [
    {
      required: true,
      message: '请填写课程时数',
      trigger: 'blur',
      type: 'number',
    },
  ],
  grade: [
    {
      required: true,
      message: '请填写学分数',
      trigger: 'blur',
      type: 'number',
    },
  ],
  classProperty: [
    {
      required: true,
      message: '请填写课程性质',
      trigger: 'blur',
    },
  ],
}
