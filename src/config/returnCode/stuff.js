export const ADDSTUFF = {
  success: {
    code: 200,
    msg: '添加成功',
  },
  error: {
    code: 201,
    msg: '添加失败',
  },
  tokenError: {
    code: 202,
    msg: 'token过期或错误',
  },
  serveError: {
    code: 301,
    msg: '服务器内部异常，请联系管理员',
  },
}

export const DELETESTUFF = {
  success: {
    code: 200,
    msg: '删除成功',
  },
  error: {
    code: 201,
    msg: '删除失败,教师编号不存在',
  },
  tokenError: {
    code: 202,
    msg: 'token过期或错误',
  },
  serveError: {
    code: 301,
    msg: '服务器内部异常，请联系管理员',
  },
}

export const UPDATESTUFF = {
  success: {
    code: 200,
    msg: '修改成功',
  },
  error: {
    code: 201,
    msg: '修改失败,教师编号不存在',
  },
  tokenError: {
    code: 202,
    msg: 'token过期或错误',
  },
  serveError: {
    code: 301,
    msg: '服务器内部异常，请联系管理员',
  },
}

export const GETALLSTUFF = {
  success: {
    code: 200,
    msg: '查询成功',
  },
  error: {
    code: 201,
    msg: '查询失败',
  },
  tokenError: {
    code: 202,
    msg: 'token过期或错误',
  },
  serveError: {
    code: 301,
    msg: '服务器内部异常，请联系管理员',
  },
}

export const GETSTUFFBYNAME = {
  success: {
    code: 200,
    msg: '查询成功',
  },
  error: {
    code: 201,
    msg: '查询失败',
  },
  tokenError: {
    code: 202,
    msg: 'token过期或错误',
  },
  serveError: {
    code: 301,
    msg: '服务器内部异常，请联系管理员',
  },
}
