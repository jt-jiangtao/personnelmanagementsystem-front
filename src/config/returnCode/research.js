export const ADDRESEARCH = {
  success: {
    code: 200,
    msg: '添加成功',
  },
  error: {
    code: 201,
    msg: '添加失败',
  },
  tokenError: {
    code: 202,
    msg: 'token过期或错误',
  },
  serveError: {
    code: 301,
    msg: '服务器内部异常，请联系管理员',
  },
}

export const DELETERESEARCH = {
  success: {
    code: 200,
    msg: 's删除成功',
  },
  error: {
    code: 201,
    msg: '删除失败，科研信息编号不存在',
  },
  tokenError: {
    code: 202,
    msg: 'token过期或错误',
  },
  serveError: {
    code: 301,
    msg: '服务器内部异常，请联系管理员',
  },
}

export const GETALLRESEARCH = {
  success: {
    code: 200,
    msg: '查询成功',
  },
  error: {
    code: 201,
    msg: '查询失败',
  },
  tokenError: {
    code: 202,
    msg: 'token过期或错误',
  },
  serveError: {
    code: 301,
    msg: '服务器内部异常，请联系管理员',
  },
}
