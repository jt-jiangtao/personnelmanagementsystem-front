export const LOGIN = {
  success: {
    code: 200,
    msg: '登录成功',
  },
  error: {
    code: 201,
    msg: '用户名或密码错误',
  },
  serveError: {
    code: 301,
    msg: '服务器内部异常，请联系管理员',
  },
}

export const REGISTER = {
  success: {
    code: 200,
    msg: '登录成功',
  },
  error: {
    code: 201,
    msg: '邮箱不存在',
  },
  serveError: {
    code: 301,
    msg: '服务器内部异常，请联系管理员',
  },
}

export const SENDCOED = {
  success: {
    code: 200,
    msg: '登录成功',
  },
  error: {
    code: 201,
    msg: '邮箱未注册',
  },
  serveError: {
    code: 301,
    msg: '服务器内部异常，请联系管理员',
  },
}

export const RESETPASS = {
  success: {
    code: 200,
    msg: '登录成功',
  },
  error: {
    code: 201,
    msg: '邮箱或验证码错误',
  },
  serveError: {
    code: 301,
    msg: '服务器内部异常，请联系管理员',
  },
}
